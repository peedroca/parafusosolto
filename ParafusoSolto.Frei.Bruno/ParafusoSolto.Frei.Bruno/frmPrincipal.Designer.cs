﻿namespace ParafusoSolto.Frei.Bruno
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnTelas = new System.Windows.Forms.Panel();
            this.btnEstoque = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnProd = new System.Windows.Forms.Button();
            this.btnFornecedor = new System.Windows.Forms.Button();
            this.btnAtuaEstoque = new System.Windows.Forms.Button();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip3 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip4 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnTelas
            // 
            this.pnTelas.BackgroundImage = global::ParafusoSolto.Frei.Bruno.Properties.Resources.PlanoDeFundo;
            this.pnTelas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnTelas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnTelas.Location = new System.Drawing.Point(270, 15);
            this.pnTelas.Name = "pnTelas";
            this.pnTelas.Size = new System.Drawing.Size(500, 380);
            this.pnTelas.TabIndex = 0;
            // 
            // btnEstoque
            // 
            this.btnEstoque.BackColor = System.Drawing.Color.Chocolate;
            this.btnEstoque.BackgroundImage = global::ParafusoSolto.Frei.Bruno.Properties.Resources.box_21467;
            this.btnEstoque.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnEstoque.FlatAppearance.BorderSize = 0;
            this.btnEstoque.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEstoque.Location = new System.Drawing.Point(12, 211);
            this.btnEstoque.Name = "btnEstoque";
            this.btnEstoque.Size = new System.Drawing.Size(120, 89);
            this.btnEstoque.TabIndex = 1;
            this.btnEstoque.UseVisualStyleBackColor = false;
            this.btnEstoque.Click += new System.EventHandler(this.btnEstoque_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ParafusoSolto.Frei.Bruno.Properties.Resources.LogoParafusoSolto;
            this.pictureBox1.Location = new System.Drawing.Point(12, 15);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(245, 190);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.AutomaticDelay = 0;
            // 
            // btnProd
            // 
            this.btnProd.BackColor = System.Drawing.Color.Chocolate;
            this.btnProd.BackgroundImage = global::ParafusoSolto.Frei.Bruno.Properties.Resources.ark_productbox_arc_6255;
            this.btnProd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnProd.FlatAppearance.BorderSize = 0;
            this.btnProd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProd.Location = new System.Drawing.Point(137, 211);
            this.btnProd.Name = "btnProd";
            this.btnProd.Size = new System.Drawing.Size(120, 89);
            this.btnProd.TabIndex = 1;
            this.btnProd.UseVisualStyleBackColor = false;
            this.btnProd.Click += new System.EventHandler(this.btnProd_Click);
            // 
            // btnFornecedor
            // 
            this.btnFornecedor.BackColor = System.Drawing.Color.Chocolate;
            this.btnFornecedor.BackgroundImage = global::ParafusoSolto.Frei.Bruno.Properties.Resources.customer_person_people_man_user_client_1629;
            this.btnFornecedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnFornecedor.FlatAppearance.BorderSize = 0;
            this.btnFornecedor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFornecedor.Location = new System.Drawing.Point(12, 306);
            this.btnFornecedor.Name = "btnFornecedor";
            this.btnFornecedor.Size = new System.Drawing.Size(120, 89);
            this.btnFornecedor.TabIndex = 1;
            this.btnFornecedor.UseVisualStyleBackColor = false;
            this.btnFornecedor.Click += new System.EventHandler(this.btnFornecedor_Click);
            // 
            // btnAtuaEstoque
            // 
            this.btnAtuaEstoque.BackColor = System.Drawing.Color.Chocolate;
            this.btnAtuaEstoque.BackgroundImage = global::ParafusoSolto.Frei.Bruno.Properties.Resources.Box_1_35524;
            this.btnAtuaEstoque.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnAtuaEstoque.FlatAppearance.BorderSize = 0;
            this.btnAtuaEstoque.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtuaEstoque.Location = new System.Drawing.Point(137, 306);
            this.btnAtuaEstoque.Name = "btnAtuaEstoque";
            this.btnAtuaEstoque.Size = new System.Drawing.Size(120, 89);
            this.btnAtuaEstoque.TabIndex = 1;
            this.btnAtuaEstoque.UseVisualStyleBackColor = false;
            this.btnAtuaEstoque.Click += new System.EventHandler(this.btnAtuaEstoque_Click);
            // 
            // toolTip2
            // 
            this.toolTip2.AutomaticDelay = 0;
            // 
            // toolTip3
            // 
            this.toolTip3.AutomaticDelay = 0;
            // 
            // toolTip4
            // 
            this.toolTip4.AutomaticDelay = 0;
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ParafusoSolto.Frei.Bruno.Properties.Resources.PlanoDeFundo;
            this.ClientSize = new System.Drawing.Size(784, 411);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnAtuaEstoque);
            this.Controls.Add(this.btnFornecedor);
            this.Controls.Add(this.btnProd);
            this.Controls.Add(this.btnEstoque);
            this.Controls.Add(this.pnTelas);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Parafuso Solto LTDA";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnTelas;
        private System.Windows.Forms.Button btnEstoque;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnProd;
        private System.Windows.Forms.Button btnFornecedor;
        private System.Windows.Forms.Button btnAtuaEstoque;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.ToolTip toolTip3;
        private System.Windows.Forms.ToolTip toolTip4;
    }
}

