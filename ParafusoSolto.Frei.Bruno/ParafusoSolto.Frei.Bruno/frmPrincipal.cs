﻿using ParafusoSolto.Frei.Bruno.Telas;
using ParafusoSolto.Frei.Bruno.Telas.Estoque;
using ParafusoSolto.Frei.Bruno.Telas.Fornecedor;
using ParafusoSolto.Frei.Bruno.Telas.Produto;
using ParafusoSolto.Frei.Bruno.Validacoes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParafusoSolto.Frei.Bruno
{
    public partial class frmPrincipal : Form
    {
        public frmPrincipal()
        {
            InitializeComponent();
            toolTip1.SetToolTip(btnEstoque, "Estoque");
            toolTip2.SetToolTip(btnProd, "Produtos");
            toolTip3.SetToolTip(btnFornecedor, "Fornecedor");
            toolTip4.SetToolTip(btnAtuaEstoque, "AtualizarEstoque");

            frmInicial screen = new frmInicial();
            LoadScreen(screen);
        }
        
        private void LoadScreen(UserControl screen)
        {
            if (pnTelas.Controls.Count == 1)
                pnTelas.Controls.RemoveAt(0);
            pnTelas.Controls.Add(screen);
        }

        private void btnEstoque_Click(object sender, EventArgs e)
        {
            frmConsEstoque screen = new frmConsEstoque();
            LoadScreen(screen);
        }

        private void btnProd_Click(object sender, EventArgs e)
        {
            frmCadNovoProd screen = new frmCadNovoProd();
            LoadScreen(screen);
        }

        private void btnFornecedor_Click(object sender, EventArgs e)
        {
            frmConsForn screen = new frmConsForn();
            LoadScreen(screen);
        }

        private void btnAtuaEstoque_Click(object sender, EventArgs e)
        {
            frmAtuaEstoque screen = new frmAtuaEstoque();
            LoadScreen(screen);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            frmInicial screen = new frmInicial();
            LoadScreen(screen);
        }
    }
}
